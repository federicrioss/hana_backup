#!/usr/bin/python3

##################################################
## Hana Backup Automation
##################################################
## 
##################################################
## Author: Federico Rios
## Copyright: Copyright 2020, Hana Backup Module
## Credits: 
## License: GPL
## Version: 0.1.1
## Mmaintainer: Federico Rios
## Email: frios@mtzsistemas.com
## Status: Prod
##################################################

import datetime
import sys, os
import glob
import logging
import configparser
import calendar

from pathlib import Path
import send_email as SM

#Constant definition for the script use
DATE = datetime.datetime.now().strftime("%d_%m_%Y")
BASE_DIR = os.path.dirname(os.path.abspath(__file__))

config = configparser.ConfigParser()
config.read(os.path.join(BASE_DIR, 'settings.cfg'))

BACKUP_PREFIX = config['BACKUP']['prefix']
BACKUP_HANA_ROOT_PATH = config['BACKUP']['hana_root']

BACKUP_REMOTE_PATH = config['BACKUP']['remote_path']

LOG_FILE="{}/logs/{}.log".format(BASE_DIR, DATE)



if not os.path.exists('{}/logs'.format(BASE_DIR)):
    os.system('mkdir {}/logs'.format(BASE_DIR))

logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', filename=LOG_FILE,level=logging.DEBUG)
def backup_prefix():
    global BACKUP_REMOTE_PATH
    global ZIP_NAME
    global BACKUP_TITLE
    if config.has_option('BACKUP','dfs_enable'):
        last_day = int(calendar.monthrange(int(datetime.datetime.now().strftime("%Y")), int(datetime.datetime.now().strftime("%m")))[1])
        week_day = int(datetime.datetime.now().strftime("%w"))
        current_day = int(datetime.datetime.now().strftime("%-d"))
        if((last_day == current_day) or (last_day - current_day < 3 and week_day == 5)):
            prefiiix_name =  datetime.datetime.now().strftime("%B")
            backup_folder = 'MONTH'
        else:
            if datetime.datetime.now().strftime("%w") in [1,2,3,4]:
                backup_folder = 'DAY'
            else:
                backup_folder = 'WEEK'
            prefiiix_name = datetime.datetime.now().strftime("%A")
    BACKUP_REMOTE_PATH += '/{}'.format(backup_folder)
    if config.has_option('BACKUP','zip_temp'):
        ZIP_NAME = '{}/{}_{}_{}.zip'.format(config['BACKUP']['zip_temp'], BACKUP_PREFIX, prefiiix_name, DATE)
    else:
        ZIP_NAME = '{}/{}_{}_{}.zip'.format(BASE_DIR, BACKUP_PREFIX, prefiiix_name, DATE)
    BACKUP_TITLE = '{} {}'.format(BACKUP_PREFIX, backup_folder)

def zip_name_crate():
    global ZIP_NAME
    if config.has_option('BACKUP','zip_temp'):
        ZIP_NAME = '{}/{}_{}.zip'.format(config['BACKUP']['zip_temp'], BACKUP_PREFIX, DATE)
    else:
        ZIP_NAME = '{}/{}_{}.zip'.format(BASE_DIR, BACKUP_PREFIX, DATE)
    logging.info('Using ZIP tmp: {}'.format(ZIP_NAME))

def send_email(status, color):
    _date = datetime.datetime.now().strftime("%A %d %B %Y, %H:%M:%S")
    with open(LOG_FILE, 'r') as file:
        _data_log = file.read().replace('\n', '<br>')
    data = {
        'backup_type': BACKUP_TITLE,
        'backup_host': config['BACKUP']['job_name'],
        'status': status,
        'full_date_time': _date,
        'detail_log': _data_log,
        'status_background': color
    }
    SM.sendMail(data, 'soporte@mtzsistemas.com', '{} {}'.format(status, config['BACKUP']['job_name']))

def terminate(error):
    logging.critical(error)
    send_email('Failed', 'red')
    sys.exit()

def run_command(command):
    _command = '{} >> {}'.format(command, LOG_FILE)
    rtn = os.system(_command)
    return True if rtn == 0 else False


def get_time():
    return datetime.datetime.now().strftime("%H_%M_%S")

def test_path(path):
    if path[-1]=='*':
        return bool(len(glob.glob(path)) > 0)
    else:
        return os.path.exists(path)

def delete_path(path, message):
    if path[-1]=='*':
        _result = run_command('rm -f ' + path)
    else:
        _result = run_command('rm -Rf ' + path)
    if _result:
        logging.info('{} deleted successiful'.format(message))
    else:
        logging.error('Has been an error deleting {}'.format(message))

def backup_create():
    logging.info('Sending the command to create the backup')
    _command = '/usr/sap/hdbclient/hdbsql -U BACKUP "backup data using file (\'{}\')"'.format(BACKUP_PREFIX)
    _result = run_command(_command)
    if _result:
        logging.info('Backup files created successfully')
    else:
        terminate('Backup files can’t be created')

def backup_zip():
    logging.info('Starting the zipping process')
    _command = 'cd {};zip -mT {} data/* log/*'.format(BACKUP_HANA_ROOT_PATH, ZIP_NAME)
    _result = run_command(_command)
    if _result:
        logging.info('ZIP file created successfully')
    else:
        _result = run_command('rm -f {}'.format(ZIP_NAME))
        terminate('ZIP file can’t be created')

def backup_send():
    if (config['BACKUP']['copy_to'] == 'ssh'):
        logging.info('Sending file to remote host')
        BACKUP_FULL_PATH = '{}:{}'.format(config['BACKUP']['remote_host'], BACKUP_REMOTE_PATH) 
    _command = 'rsync -azh --remove-source-files {} {}'.format(ZIP_NAME, BACKUP_FULL_PATH)
    _result = run_command(_command)
    if _result:
        logging.info('Send ZIP file to remote host correctly')
    else:
        terminate('ZIP file can’t be send to remote host')

def main ():
    backup_prefix()
    _backup_file = BACKUP_HANA_ROOT_PATH + 'data/' + BACKUP_PREFIX + '*'
    if (test_path(_backup_file)):
        logging.info('Deleting old backup files')
        delete_path(_backup_file, 'Old Backup Files')
    backup_create()
    backup_zip()
    backup_send()
    send_email('Success', 'green')

def send_test(status, color):
    _date = datetime.datetime.now().strftime("%A %d %B %Y, %H:%M:%S")
    with open(LOG_FILE, 'r') as file:
        _data_log = file.read().replace('\n', '<br>')
    data = {
        'backup_type': BACKUP_PREFIX,
        'backup_host': config['BACKUP']['job_name'],
        'status': status,
        'full_date_time': _date,
        'detail_log': 'Mandamos porquerias',
        'status_background': color
    }
    SM.sendMail(data, 'frios@mtzsistemas.com', '{} {}'.format(status, config['BACKUP']['job_name']))

if __name__ == '__main__':
    main()
