#!/usr/bin/python3

##################################################
## Auxiliary modules to send emails
##################################################
## 
##################################################
## Author: Federico Rios
## Copyright: Copyright 2020, SendEmail Module
## Credits: 
## License: GPL
## Version: 0.1.1
## Mmaintainer: Federico Rios
## Email: frios@mtzsistemas.com
## Status: Prod
##################################################

# Libraryes
import codecs
import configparser
import smtplib
import os

# Modules
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from string import Template

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# Read the setting file.
config = configparser.ConfigParser()
config.read(os.path.join(BASE_DIR, 'settings.cfg'))

#Open the templeate files to render the info
email_html_path = os.path.join(BASE_DIR, 'templates', 'email.html')
email_txt_path = os.path.join(BASE_DIR, 'templates', 'email.txt')

#Load the files templates to be render
file_html = codecs.open(email_html_path, 'r', encoding='utf8')
file_text = codecs.open(email_txt_path, 'r', encoding='utf8')
html_render = Template(file_html.read())
text_render = Template(file_text.read())

#Get a dict with the info to render, and the recipient
def sendMail(data, mail_to, subject): 
    #Proccess the templates
    html = html_render.substitute(data)
    text = text_render.substitute(data)
    # Make the email Object
    email_content = MIMEMultipart('alternative')
    email_content.set_charset('utf-8')
    email_content['To'] = mail_to
    email_content['From'] = config['EMAIL']['mail_from']
    email_content['Subject'] = subject
    # Attach the email files
    part1 = MIMEText(text, 'plain', 'utf-8')
    part2 = MIMEText(html, 'html', 'utf-8')
    email_content.attach(part1)
    email_content.attach(part2)
    # Send the email
    mail = smtplib.SMTP_SSL(config['EMAIL']['smtp_server'], 465)
    mail.ehlo()
    mail.login(config['EMAIL']['smtp_user'],config['EMAIL']['smtp_pass'])
    mail.sendmail(config['EMAIL']['mail_from'], mail_to, email_content.as_string())
    mail.close()

def main ():
    data = {
        'test': 'Vamos que vamos',
    }
    sendMail(data, 'frios@mtzsistemas.com', 'Testeando ando')


if __name__ == '__main__':
    main()